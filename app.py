from flask import Flask,render_template,url_for,request,flash,redirect,session
from flask_mysqldb import MySQL
from threading import Thread
from forms import LoginForm, forgotPasswordForm,incidentTicketForm,requirementTicketForm,userProgressForm,userTicket,adminProgressFrom,managementProgressFrom
from flask_login import LoginManager,logout_user,current_user, login_user,login_required
from flask_mail import Mail,Message

app=Flask(__name__)
login = LoginManager(app)


app.config['MAIL_SERVER']='smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'info.wavicledata@gmail.com'
app.config['MAIL_PASSWORD'] = 'wavicle1234'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True



app.config['SECRET_KEY']='ac61edf64fe6feda631e8193c0675d39'
app.config['MYSQL_HOST'] = 'courses.c7cbcmuhcbn1.us-east-1.rds.amazonaws.com'
app.config['MYSQL_USER'] = 'wavicledev'
app.config['MYSQL_PASSWORD'] = 'wavicle1234'
app.config['MYSQL_DB'] = 'ticketing'
app.config['MYSQL_CURSORCLASS']='DictCursor'

mysql = MySQL(app)
mail = Mail(app)



@login.user_loader
def load_user(id):
    cur = mysql.connection.cursor()
    cur.execute('select * from emp_details where emp_id = %s', [id])
    mysql.connection.commit()

    return cur.fetchall()

@app.route('/home')
def dashboard():
        current_user_id = session['employee_id']
        cur = mysql.connection.cursor()
        cur.execute('select * from emp_details where emp_id = %s', [current_user_id])
        mysql.connection.commit()
        req_tab = cur.fetchall()
        success_name=req_tab[0]['emp_name']
        return render_template('./user/dashboard.html',success_name=success_name)



@app.route('/', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    cur = mysql.connection.cursor()
    cur.execute('''select * from emp_details;''')
    mysql.connection.commit()
    user_detail = cur.fetchall()

    if form.validate_on_submit():
        for i in range(0,len(user_detail),1):
            if form.employee_id.data==user_detail[i]['emp_id'] and form.password.data==user_detail[i]['emp_psswrd']:
                success_name=user_detail[i]['emp_name']
                session['employee_id'] = form.employee_id.data

                cur = mysql.connection.cursor()
                cur.execute('select * from emp_details where emp_id = %s', [form.employee_id.data])
                mysql.connection.commit()
                req_tab = cur.fetchall()
                session['employee_email_id']=req_tab[0]['emp_mail']

                if req_tab[0]['departments'] == 'IT Support':
                    return redirect(url_for('admin'))

                elif form.password.data == '1234' and req_tab[0]['departments'] == 'Developers':
                    flash(f'you have been logged in! {success_name}','success')
                    flash(f'User can change the password yet it is default one.','warning')
                    return redirect(url_for('dashboard',success_name=success_name))

                elif req_tab[0]['departments'] == 'OM':
                    return redirect(url_for('operation_manager'))

                else:
                    flash(f'you have been logged in! {success_name}', 'success')
                    return redirect(url_for('dashboard',success_name=success_name))

        flash('Login Unsuccessful. Please check username and password','danger')
    return render_template('./login/login.html', form=form, title="Login")

@app.route('/user_tickets',methods = ['POST', 'GET'])
def user_tickets():
    form=userTicket()
    print('User Ticket',form.ticket.data)



    current_user_id = session['employee_id']
    cur = mysql.connection.cursor()
    cur.execute('select * from inc_tic where emp_id = %s',[current_user_id])
    mysql.connection.commit()
    inc_tab = cur.fetchall()

    cur = mysql.connection.cursor()
    cur.execute('select * from req_tic where emp_id = %s', [current_user_id])
    mysql.connection.commit()
    req_tab = cur.fetchall()
    return render_template('./user/user_table.html',inc_tab=inc_tab,req_tab=req_tab,form=form)



@app.route('/user_progress/<string:emp_id>/<int:ticket_id>',methods = ['POST', 'GET'])
def user_progress(ticket_id,emp_id):
    form=userProgressForm()
    print('progress_radio', form.progress_radio.data)
    s = str(ticket_id)
    print('ticket ID', s[0])
    if form.validate_on_submit():
        print('val')
        if s[0] == '1':
            cur = mysql.connection.cursor()
            cur.execute('update inc_tic set inc_status = %s where inc_tic_id = %s',
                        (form.progress_radio.data, ticket_id))
            mysql.connection.commit()

            # Email processing

            cur = mysql.connection.cursor()
            cur.execute('select * from emp_details where emp_id = %s', [emp_id])
            mysql.connection.commit()
            req_tab = cur.fetchall()

            sender = req_tab[0]['emp_mail']
            print(sender)
            recipients = sender
            html_body = render_template("./user/email.html", progress=form.progress_radio.data,ticket_id=ticket_id,emp=req_tab)
            send_email('progress update', 'tharani2797@gmail.com', recipients, 'Hi', html_body)
            send_email('progress update', 'tharani2797@gmail.com', 'tharani2797@gmail.com', 'Hi',html_body)



            flash(f'Your response is recorded.. ', 'success')



            return redirect(url_for('user_tickets'))
        else:
            cur = mysql.connection.cursor()
            cur.execute('update req_tic set rq_status_ad = %s where rq_tic_id = %s',
                        (form.progress_radio.data, ticket_id))
            mysql.connection.commit()
            print('done updating')
            flash(f'Your response is recorded.. ', 'success')
            return redirect(url_for('user_tickets'))

    return render_template('./user/progress.html',form=form,ticket_id=ticket_id)

@app.route('/admin_progress/<string:emp_id>/<int:ticket_id>',methods = ['POST', 'GET'])
def admin_progress(ticket_id,emp_id):
    form=adminProgressFrom()
    print('progress_radio', form.progress_radio.data)
    s = str(ticket_id)
    print('ticket ID', s[0])
    if form.validate_on_submit():
        print('val')
        if s[0] == '1':
            cur = mysql.connection.cursor()
            cur.execute('update inc_tic set inc_status = %s where inc_tic_id = %s',
                        (form.progress_radio.data, ticket_id))
            mysql.connection.commit()
            print('done updating')

            cur = mysql.connection.cursor()
            cur.execute('select * from emp_details where emp_id = %s', [emp_id])
            mysql.connection.commit()
            inc_tab = cur.fetchall()

            sender = inc_tab[0]['emp_mail']
            print(sender)
            recipients = sender
            html_body = render_template("./admin/email.html", progress=form.progress_radio.data, ticket_id=ticket_id,
                                        admin=inc_tab)
            send_email('progress update', 'info.wavicledata@gmail.com', recipients, 'Hi', html_body)
            send_email('progress update', 'info.wavicledata@gmail.com', 'info.wavicledata@gmail.com', 'Hi', html_body)

            if form.progress_radio.data == 'close':
                cur = mysql.connection.cursor()
                cur.execute(
                    'update report_log set tc_clsd_dt = now(),tc_final_status=%s,tc_rslvd_hr=  time(timediff(now(),tc_opn_dt))where tc_id= %s',
                    (form.progress_radio.data, ticket_id))
                mysql.connection.commit()
                print('done updating log table')
            flash(f'Your response is recorded.. ', 'success')
            return redirect(url_for('admin'))

        else:
            cur = mysql.connection.cursor()
            cur.execute('update req_tic set rq_status_ad = %s where rq_tic_id = %s',
                        (form.progress_radio.data, ticket_id))
            mysql.connection.commit()
            print('done updating')

            cur = mysql.connection.cursor()
            cur.execute('select * from emp_details where emp_id = %s', [emp_id])
            mysql.connection.commit()
            inc_tab = cur.fetchall()

            sender = inc_tab[0]['emp_mail']
            print(sender)
            recipients = sender
            html_body = render_template("./admin/email.html", progress=form.progress_radio.data, ticket_id=ticket_id,
                                        admin=inc_tab)
            send_email('progress update', 'info.wavicledata@gmail.com', recipients, 'Hi', html_body)
            send_email('progress update', 'info.wavicledata@gmail.com', 'info.wavicledata@gmail.com', 'Hi', html_body)

            if form.progress_radio.data == 'close':
                cur = mysql.connection.cursor()
                cur.execute(
                    'update report_log set tc_clsd_dt = now(),tc_final_status=%s,tc_rslvd_hr=  time(timediff(now(),tc_opn_dt))where tc_id= %s',
                    (form.progress_radio.data, ticket_id))
                mysql.connection.commit()
                print('done updating log table')
            flash(f'Your response is recorded.. ', 'success')
            return redirect(url_for('admin'))




    return render_template('./admin/progress.html',form=form,ticket_id=ticket_id)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))

@app.route('/management_progress/<string:emp_id>/<int:ticket_id>',methods = ['POST', 'GET'])
def management_progress(ticket_id,emp_id):
    form=managementProgressFrom()
    print(ticket_id)
    if form.validate_on_submit():
        cur = mysql.connection.cursor()
        cur.execute('update req_tic set rq_apprvl_op = %s where rq_tic_id = %s',
                    (form.progress_radio.data, ticket_id))
        mysql.connection.commit()
        print('done updating')

        cur.execute('select * from req_tic where rq_tic_id=%s',[ticket_id])
        mysql.connection.commit()
        ticket_prd=cur.fetchall()


        cur.execute('select * from emp_details where emp_id = %s', [emp_id])
        mysql.connection.commit()
        req_tab = cur.fetchall()

        sender = req_tab[0]['emp_mail']
        print(sender)
        recipients = sender
        html_body = render_template("./user/email.html",ticket_prd=ticket_prd)
        send_email('progress update', 'info.wavicledata@gmail.com', recipients, 'Hi', html_body)
        send_email('progress update', 'info.wavicledata@gmail.com', 'info.wavicledata@gmail.com', 'Hi', html_body)




        flash(f'Your response is recorded.. ', 'success')
        return redirect(url_for('operation_manager'))

    return render_template('./op_manager/progress.html',form=form,ticket_id=ticket_id)




@app.route("/operation_manager",methods = ['POST', 'GET'])
def operation_manager():
    cur = mysql.connection.cursor()
    cur.execute('''select * from req_tic;''')
    mysql.connection.commit()
    req_post = cur.fetchall()

    return render_template('./op_manager/op_manager_table.html',req_post=req_post)



@app.route("/admin")
def admin():
    if request.method == "GET":
        cur = mysql.connection.cursor()
        cur.execute('''select * from inc_tic;''')
        mysql.connection.commit()
        post = cur.fetchall()

        cur.execute('''select * from req_tic;''')
        mysql.connection.commit()
        req_post = cur.fetchall()

        cur.execute('''select * from report_log;''')
        mysql.connection.commit()
        log_tb = cur.fetchall()
        # print(log_tb)
        return render_template('./admin/admin_dashboard.html', post=post,req_post=req_post,log_tb=log_tb)

@app.route("/forgot_password",methods=['GET', 'POST'])
def forgot_password():
    form=forgotPasswordForm()
    if form.validate_on_submit():
        cur = mysql.connection.cursor()
        cur.execute('update emp_details set emp_psswrd=%s where emp_mail=%s and emp_id= %s',(form.password.data, form.email.data,form.employee_id.data))
        mysql.connection.commit()
        print(cur.rowcount)
        if cur.rowcount>0:
            flash(f'Password created succssfully!','success')
            return redirect(url_for('dashboard'))
        else:
            flash('Verification Unsuccessful. Please check email id and phone number', 'danger')


    return render_template('./login/forgotpassword.html',form=form)


@app.route('/requirement_ticket',methods=['GET', 'POST'])
def req_ticket():
    form = requirementTicketForm()
    if form.validate_on_submit():
        current_user_id = session['employee_id']
        print('Incident Id:', form.req_id.data, 'Priority level: ', form.priority.data, 'Address:', form.address.data,'', current_user_id,'',form.req_name.data)

        cur = mysql.connection.cursor()
        cur.execute('insert into req_tic(emp_id,rq_id,rq_prdct,req_Prty,req_cmt,emp_name) values (%s,%s,%s,%s,%s,%s)',(current_user_id, form.req_id.data, form.req_name.data, form.priority.data, form.address.data,session['employee_name']))
        mysql.connection.commit()

        last = mysql.connection.cursor()
        last.execute('select * from req_tic where rq_tic_id=(SELECT LAST_INSERT_ID());')
        mysql.connection.commit()
        last_detail = last.fetchall()
        print(last_detail)

        emp = mysql.connection.cursor()
        emp.execute('select * from emp_details where emp_id = %s', [current_user_id])
        mysql.connection.commit()
        emp_email = emp.fetchall()

        log = mysql.connection.cursor()
        log.execute('''insert into report_log(tc_id,fn_id,fn_name,tc_pre_status,tc_opn_dt) select rq_tic_id,rq_id,rq_prdct,rq_status_ad,dte from req_tic
        where rq_tic_id not in (select rq_tic_id from req_tic,report_log where req_tic.rq_tic_id = report_log.tc_id);
                ''')
        mysql.connection.commit()
        session['employee_email_id']=emp_email[0]['emp_mail']

        sender = emp_email[0]['emp_mail']
        print(sender)
        recipients = emp_email[0]['emp_mail']
        html_body = render_template("./ticketing/email.html", user_rq=last_detail)
        send_email('New Requirement Ticket Generate', 'info.wavicledata@gmail.com', recipients, 'Hi', html_body)
        send_email('New Requirement Ticket Generate', 'info.wavicledata@gmail.com', 'info.wavicledata@gmail.com', 'Hi', html_body)

        flash(f'Your response is recorded.. ', 'success')
        return render_template('./ticketing/req_ticket.html',form=form)

    return render_template('./ticketing/req_ticket.html', title='requirement ticket',form=form)



def send_async_email(app, msg):
    with app.app_context():
        mail.send(msg)


def send_email(subject, sender, recipients, text_body,html_body):
    msg = Message(subject, sender=sender, recipients=[recipients])
    msg.body = text_body
    msg.html = html_body
    thr = Thread(target=send_async_email, args=[app, msg])
    thr.start()

@app.route('/incident_ticket',methods=['GET', 'POST'])
def inc_ticket():
    form=incidentTicketForm()
    if form.validate_on_submit():
        current_user_id=session['employee_id']
        print('Incident Id:',form.inc_id.data,'Priority level: ',form.priority.data,'Address:',form.address.data,'',current_user_id)
        cur = mysql.connection.cursor()
        cur.execute('insert into inc_tic(emp_id,inc_id,inc_name,inc_Prty,inc_cmt,emp_name) values (%s,%s,%s,%s,%s,%s)',(current_user_id,form.inc_id.data,form.inc_name.data,form.priority.data, form.address.data,session['employee_name']))
        mysql.connection.commit()

        last=mysql.connection.cursor()
        last.execute('select * from inc_tic where inc_tic_id=(SELECT LAST_INSERT_ID());')
        mysql.connection.commit()
        last_detail=last.fetchall()
        print(last_detail)

        emp=mysql.connection.cursor()
        emp.execute('select * from emp_details where emp_id = %s',[current_user_id])
        mysql.connection.commit()
        emp_email=emp.fetchall()

        log=mysql.connection.cursor()
        log.execute(''' insert into report_log(tc_id,fn_id,fn_name,tc_pre_status,tc_opn_dt) select inc_tic_id,inc_id,inc_name,inc_status,dte from inc_tic
        where inc_tic_id not in (select inc_tic_id from inc_tic , report_log where inc_tic.inc_tic_id = report_log.tc_id);
        ''')
        mysql.connection.commit()

        sender=emp_email[0]['emp_mail']
        print(sender)
        recipients=emp_email[0]['emp_mail']
        html_body=render_template("./ticketing/email.html",user=last_detail)
        send_email('New Incident Ticket Generate','info.wavicledata@gmail.com',recipients,'Hi',html_body)
        send_email('New Incident Ticket Generate', 'info.wavicledata@gmail.com','info.wavicledata@gmail.com', 'Hi', html_body)

        # msg = Message('New Ticket Rising', sender='tharani2797@gmail.com', recipients=['bhavadharini.marimuthu@wavicledata.com'])
        # msg.body = 'Hi,\n New ticket has been assigned to your team.Please resolve it as soon as possible'
        # mail.send(msg)

        flash(f'Your response will handle to the IT support.. ', 'success')
        return render_template('./ticketing/inc_ticket.html',form=form)
    return render_template('./ticketing/inc_ticket.html',title='incident ticket',form=form)




if __name__=='__main__ ':
    app.run(debug=True)
