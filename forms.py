from flask_wtf import FlaskForm
from wtforms import (StringField,
                    PasswordField,
                    SubmitField,
                    SelectField, IntegerField, validators,ValidationError,BooleanField,RadioField,TextAreaField)
from wtforms.validators import DataRequired, Length, Email, EqualTo
import phonenumbers
from wtforms.fields.html5 import DateField


class LoginForm(FlaskForm):
    employee_id = StringField('Employee Id',validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember=BooleanField('Remember Me')
    submit = SubmitField('Sign In')

class forgotPasswordForm(FlaskForm):
    email = StringField('Employee Email', validators=[DataRequired(), Email()])
    employee_id = StringField('Employee Id',validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField(
        'Reset Password', validators=[DataRequired(), EqualTo('password'),EqualTo('password')])
    submit = SubmitField('submit')



    # def validate_phone(form, field):
    #     if len(field.data) > 16:
    #         raise ValidationError('Invalid phone number.')
    #     try:
    #         input_number = phonenumbers.parse(field.data)
    #         if not (phonenumbers.is_valid_number(input_number)):
    #             raise ValidationError('Invalid phone number.')
    #     except:
    #         input_number = phonenumbers.parse("+1" + field.data)
    #         if not (phonenumbers.is_valid_number(input_number)):
    #             raise ValidationError('Invalid phone number.')


class incidentTicketForm(FlaskForm):
    inc_id = StringField('Id', validators=[DataRequired()])
    inc_name = StringField('Incident name', validators=[DataRequired()])
    priority = RadioField('Label', choices=[('critical' ,'critical' ),('High', 'High'), ('Medium', 'Medium'),('Low', 'Low') ])
    address = TextAreaField(u'Mailing Address', [validators.optional(), validators.length(max=200)])
    submit = SubmitField('submit')

class requirementTicketForm(FlaskForm):
    req_id = StringField('Id', validators=[DataRequired()])
    req_name = StringField('requirement name', validators=[DataRequired()])
    priority = RadioField('Label', choices=[('critical' ,'critical' ),('High', 'High'), ('Medium', 'Medium'),('Low', 'Low') ])
    address = TextAreaField(u'Mailing Address', [validators.optional(), validators.length(max=200)])
    verify = SubmitField('submit')

class userProgressForm(FlaskForm):
    progress_radio = RadioField('Label', choices=[('reopen', 'REOPEN'),('close', 'CLOSE')])
    submit = SubmitField('submit')

class userTicket(FlaskForm):
    ticket=StringField('Id', validators=[DataRequired()])
    submit=SubmitField('submit')

class adminProgressFrom(FlaskForm):
    progress_radio = RadioField('Label', choices=[('inprogress', 'INPROGRESS'), ('close', 'CLOSE')])
    submit = SubmitField('submit')

class managementProgressFrom(FlaskForm):
    progress_radio = RadioField('Label', choices=[('approve', 'APPROVE'),('reject','REJECT')])
    submit = SubmitField('submit')

